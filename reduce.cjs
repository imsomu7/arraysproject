function reduce(elements, cb, startingvalues) {
    if (!Array.isArray(elements) || cb === undefined) {
        return [];
    }
    if (startingvalues === undefined) {
        startingvalues = elements[0];
        for (let index = 1; index < elements.length; index++) {
            let list = cb(startingvalues, elements[index], index, elements);
            startingvalues = list;

        }
        return startingvalues;
    }
    for (let index = 0; index < elements.length; index++) {
        let list = cb(startingvalues, elements[index], index, elements);
        startingvalues = list;
    }
    return startingvalues;

}

module.exports = reduce;