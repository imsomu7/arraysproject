function each(elements, cb) {
    for (let index in elements) {
        cb(elements[index], index);
    }
}
module.exports = each;

