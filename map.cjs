function map(elements, cb) {
    // console.log(elements);
    if (!Array.isArray(elements) || cb === undefined) {
        return [];
    }
    let newArry = [];
    for (let index = 0; index < elements.length; index++) {
        //console.log(elements[index]);
        let item = cb(elements[index], index, elements);
        //console.log(item);
        newArry.push(item);
    }
    return newArry;
}

module.exports = map;