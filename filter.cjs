function filter(elements, cb) {
    if (!Array.isArray(elements) || cb === undefined) {
        return [];
    }
    let array = [];
    for (let index = 0; index < elements.length; index++) {
        let value = cb(elements[index],index,elements);
        if (value === true) {
            array.push(elements[index]);

        }
        // else{
        //     return [];
        // }

    }
    return array;
}
module.exports = filter;
// function filter(){}
// function cb(){}
// export {filter,cb}
