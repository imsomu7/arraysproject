function flatten(inputelements, depth = 2) {
    let outputarray = [];
    if (inputelements === [] || inputelements === undefined || typeof (inputelements) != 'object') {
        return inputelements;

    }
    recurssive(0, inputelements, outputarray, depth);
    return outputarray;
}
function recurssive(index, inputelements, outputarray, depth) {
    if (index >= inputelements.length) {
        return;
    }
    if (Array.isArray(inputelements[index]) && depth > 0) {
        // console.log(inputelements[index]);
        recurssive(0, inputelements[index], outputarray, depth - 1);
    }
    else {
        if (inputelements[index] != undefined)
            outputarray.push(inputelements[index]);

    }
    recurssive(index + 1, inputelements, outputarray, depth)
}

module.exports = flatten;
