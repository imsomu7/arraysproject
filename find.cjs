function find(elements,cb)
{
    for (let index in elements)
    {
        let value = cb(elements[index]);
        if (value === true)
        {
            return elements[index];
        }
    }
}

module.exports = find;